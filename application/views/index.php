<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Joshua F. Rogers: _Coeus</title>
	<link rel="stylesheet" href="public/css/style.css" title="" type="text/css" media="screen" charset="utf-8">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
</head>
<body>
	<div id='menu'>
		<div id='title'><a href='#'><h2>_Coeus: Joshfrogers</h2></a></div>
		<div id='desc'>Student Physicist, part-time WebDev, I love to build.</div>
		<div><a href='http://www.github.com/joshfrogers'>Github</a></div>
		<div><a href='http://www.joshfrogers.co.uk/blog'>Blog</a></div>


	</div>
	<div id='content'>
		<ul id='lifefeed'>
			<?php
			foreach ( $posts as $item ) {
				echo '<li class ="'.$item[source].'">';
				switch ( $item[source] ) {
					case 'github':
					switch ( $item[type] ) {
						case 'PushEvent':
						echo 'pushed to <a href="https://github.com/'.$item[name].'">'.$item[name].'</a>&nbsp;<span id="time"> AGO</span><blockquote>'.$item[message].'</blockquote>';
						break;

						case 'ForkEvent':
						echo 'forked <a href="https://github.com/'.$item[name].'">'.$item[name].'</a><span id="time"> AGO</span>';
						break;

						case 'FollowEvent':
						echo 'followed <a href="https://github.com/'.$item[user].'">'.$item[user].'</a><span id="time"> AGO</span>';
						break;

						case 'WatchEvent':
						echo 'watched <a href="https://github.com/'.$item[name].'">'.$item[name].'</a><span id="time"> AGO</span>';
						break;

						case 'CreateEvent':
						echo 'created a repo - <a href="https://github.com/'.$item[name].'">'.$item[name].'</a><span id="time"> AGO</span>';
						break;

						case 'MemberEvent':
						echo 'did something crazy with <a href="https://github.com/'.$item[name].'">'.$item[name].'</a><span id="time"> AGO</span>';
						break;

						default:
						echo 'balls';
						break;
					}
					break;

					case 'twitter':
					echo $item[message].'&nbsp;<span id="time"> AGO</span>';
					break;

					case 'lastfm':
					echo 'listened To <a href="'.$item[message].'">'.$item[name][artist].' - '.$item[name][trackname].' </a>&nbsp; <span id="time"> AGO</span>';
					break;

					case 'blog':
					echo 'Posted a blog <a href="'.$item[message][url].'">'.$item[name].'</a>&nbsp; <span id="time"> AGO</span>';
					break;

					default:
					echo 'fuckfuckfuck';
					break;

					echo '</li>';
				}

			}

			?>
		</ul>
	</div>
</body>
</html>
