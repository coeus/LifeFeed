<?php

class IndexController extends BaseController
{
	private $github  = 'https://api.github.com/users/joshfrogers/events?';
    private $twitter = 'https://api.twitter.com/1/statuses/user_timeline.json?callback=?&include_rts=true&include_entities=true&screen_name=_coeus&count=25';
    private $lastfm  = 'http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=joshfrogers&api_key=42c350f2187eb1bcee497d4117cb372f&limit=20&format=json';

    public function index() {
    	$this->data[0] = $this->loadFeed('github.json', $this->github );
        $this->data[1] = $this->loadFeed('twitter.json', $this->twitter );
        $this->data[2] = $this->loadFeed('lastfm.json', $this->lastfm );

		$this->registry->template->posts = $this->arrayBuilder( $this->concat() );
		$this->registry->template->show( 'index.php' );
	}

	private function arrayBuilder( $aggregate ) {
		array_multisort( array_keys( $aggregate ), SORT_DESC, $aggregate );
		unset( $aggregate[''] );
		return $aggregate;
	}

	private function concat() {
		$git = $this->data[0];
		$twit = $this->data[1];
		$last = $this->data[2];

		foreach ( $git as $item ) {
			if ( $item[payload][commits][0][message] == null ) {
				$content = ' ';
			} else {
				$content = $item[payload][commits][0][message];
			}
			if ( $item[payload][target][login] == null ) {
				$user = ' ';
			} else {
				$user = $item[payload][target][login];
			}
			$gitTime              = strtotime( $item[created_at] );
			$gitContent           = array(
				'source' => 'github',
				'name' => $item[repo][name],
				'type' => $item[type],
				'created_at' => $gitTime,
				'message' => $content,
				'id_str' => '',
				'user' => $user
			);
			$streamData[$gitTime] = $gitContent;
		}
		foreach ( $last['recenttracks'] as $item ) {
			for ( $i = 0; $i < 20; $i++ ) {
				$lastFmTime              = $item[$i]['date']['uts'];
				$lastFmContent           = array(
					'source' => 'lastfm',
					'name' => array(
						'trackname' => $item[$i][name],
						'artist' => $item[$i][artist]['#text']
					),
					'type' => '',
					'created_at' => $lastFmTime,
					'message' => $item[$i][url],
					'id_str' => '',
					'user' => ''
				);
				$streamData[$lastFmTime] = $lastFmContent;
			}
		}
		foreach ( $twit as $item ) {
			$twitterTime              = strtotime( $item[created_at] );
			$twitterContent           = array(
				'source' => 'twitter',
				'name' => '',
				'type' => '',
				'created_at' => $twitterTime,
				'message' => $item[text],
				'id_str' => $item[id_str],
				'user' => $item[user][screen_name]
			);
			$streamData[$twitterTime] = $twitterContent;
		}
		return $streamData;
	}

	 private function loadFeed($file,$url) {
	 	$feedcache = new FeedCache($file,$url);
        $json = $feedcache->get_data();
        $json = json_decode( $json, true );
        return $json;
    }
}
