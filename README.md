LifeFeed
=

Php script Used to generate the json feed for my personal site, based on the work of Chromakode.

Requirements
-

Erunyon's FeedCache script  - [https://github.com/erunyon/FeedCache](https://github.com/erunyon/FeedCache)
Last Fm developer api key - [http://last.fm/api](http://last.fm/api)
A dedicated cache folder in the root of your public html file, as per the requirements of Erunyon's script.

Usage
-

To use the example page included - replace the arguments in lifefeed.php with your respective details.

Note
-

Not intended for widespread use - more of a proof of concept implementation of Chromakode's been/wake scripts, but using php/js with no database.
